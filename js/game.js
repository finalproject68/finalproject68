// create a new scene named "Game"
let gameScene = new Phaser.Scene('Game');
var points = 0;
var lives = 3;
var player;
var keyD;
var group1;
let group2;

// some parameters for our scene
gameScene.init = function() {
  this.playerSpeed = 3;
 // this.playerMinY = 250;
 // this.playerMaxY = 250;

};

// load asset files for our game
gameScene.preload = function() {
  this.load.image('background', 'assets/background.jpg');
  this.load.image('player', 'assets/ball.png');
  this.load.image('spike', 'assets/spikes.png');
  this.load.image('platform', 'assets/platform.png');
  this.load.image('obstacle', 'assets/obstacle.png');
  this.load.image('star', 'assets/star.png');

};


// executed on every frame (60 times per second)
gameScene.create = function() {
  
  ////background movement
  this.bg = this.add.tileSprite(0,0,this.sys.game.config.width*4, this.sys.game.config.height*4, 'background');
  // bg.setOrigin(0,0);
  this.bg.setScale(0.5);
  
  //platforms = this.physics.add.staticGroup();

  //platforms.create(300,100, 'platform');

  timeText = this.add.text(20,20);
  pointText = this.add.text(20,40);
  liveText = this.add.text(480,20);

  this.player = this.physics.add.sprite(107,250, 'player');

  this.player.setScale(0.2);
  
  this.player.setBounce(0.2);

  this.player.body.allowGravity = true;

  this.player.setCollideWorldBounds(true);

 // this.physics.add.collider(this.player, platform);

 // this.physics.add.collider(player, platform);

  
  graphics = this.add.graphics({ lineStyle: { width: 2, color: 0x0000aa }, fillStyle: { color: 0xaa0000 }});

  rect = new Phaser.Geom.Rectangle(0, 300, 640, 300);

  keyD = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
  keyA = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
  keySpace = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);

  group1 = this.add.group({
      defaultKey: 'spike',
   maxSize: 5,
    createCallback: function (spike) {
        spike.setName('spikes' + this.getLength());
        console.log('Created', spike.name);
    },
    removeCallback: function (spike) {
        console.log('Removed', spike.name);
    }

  });

  //this.spike.setScale(0.5);
  group2 = this.add.group({
    defaultKey: 'star',
    maxSize: 5,
    createCallback: function (star) {
        star.setName('star' + this.getLength());
        console.log('Created', star.name);
    },
    removeCallback: function (star) {
        console.log('Removed', star.name);
    }

  });

  this.time.addEvent({
    delay: 2000,
    loop: true,
    callback: this.addSpike
  });

  this.time.addEvent({
    delay: 500,
    loop: true,
    callback: this.addStar
  });

};

// executed on every frame (60 times per second)
gameScene.update = function() {

  
    //console.log(player.y);

  //Here is were all your game logic and animations goes
  this.bg.tilePositionX = this.bg.tilePositionX + 4;

  if (keyD.isDown){
    this.player.x += this.playerSpeed;
    console.log("Me presionaron key D!");
  }


  if (keyA.isDown){
    this.player.x -= this.playerSpeed;
    console.log("Me presionaron key D!");
  }

  if (keySpace.isDown){
    this.player.setVelocityY(-300);
  }

 // else if (keyA.isDown){
    //player.x -= this.playerSpeed;
    //console.log("Me presionaron key D!");
  //}
 // else if (keySpace.isDown){
   // player.setVelocityY(-200);

    //console.log("me presionaron space bar");
  //}

  timeText.setText('time: ' + this.sys.game.loop.time.toString());

  Phaser.Actions.IncX(group1.getChildren(), -1);
  Phaser.Actions.IncX(group2.getChildren(), -1);

    group1.children.iterate(function(spike){
        if (spike.x < 0) {
            group1.killAndHide(spike);
        }
      });

      group2.children.iterate(function(star){
        if (star.x < 0) {
            group2.killAndHide(star);
        }
      });  
      
      // enemy movement and collision
      let spike = group1.getChildren();
      let numSpike = spike.length;
    
      for (let i = 0; i < numSpike; i++) {
        // enemy collision
        if (Phaser.Geom.Intersects.RectangleToRectangle(this.player.getBounds(), spike[i].getBounds())) {
          this.livesCounter();  
          break;
        }

      }

  if (keySpace.isDown){
    this.player.setVelocityY(-200);
  console.log("me presionaron space bar");
  }
  timeText.setText('time: ' + this.sys.game.loop.time.toString());
  pointText.setText('points: ' + points);
  liveText.setText('lives: ' + lives); 
  }

;

//player collision with star 

//if (Phaser.Geom.Intersects.RectangleToRectangle(this.player.getBounds(), "enemy".getBounds())){
//  this.gamePoint();
//}


//player collision with obstacle 
  //graphics.clear();
  //graphics.strokeRectShape(rect);





gameScene.activateSpike = function(spike){
    spike.setActive(true).setVisible(true);
};

gameScene.activateStar = function(star){
  console.log("Acivate starf function");
  console.log("Star: "+JSON.stringify(star));
  star.setActive(true).setVisible(true);
};

gameScene.addSpike = function(){
  var spike = group1.get(Phaser.Math.Between(640, 840), 300);

  if (!spike) return; // None free

  gameScene.activateSpike(spike);
 
};

gameScene.addStar = function(){
  console.log("add starf function");
  var star = group2.get(Phaser.Math.Between(300, 350), 500);
  console.log("Star: "+JSON.stringify(star));
  if (!star) return; // None free

  gameScene.activateStar(star);
};

gameScene.gameOver = function() {
  // flag to set player is dead
  this.isPlayerAlive = false;
 
};

gameScene.livesCounter = function(){
  lives--;

  //var lives = [1];


}

gameScene.gamePoint = function() {
  this.isPlayerAlive = true;
  points++;
};

// our game's configuration
let config = {
  type: Phaser.AUTO,
  width: 640,
  height: 360,
  scene: gameScene,
  physics: {
    default: 'arcade',
    arcade: {
        gravity: { y: 450 },
        debug: false
    }
}
};

// create the game, and pass it the configuration
let game = new Phaser.Game(config);
